﻿using System;
using System.Xml.Linq;
using System.IO;
using System.Reflection;

namespace HTML_Builder.Service
{
    public class Configurator
    {
        static string programFolder = Directory.GetParent(Assembly.GetExecutingAssembly().Location).ToString();
        static string configPath = Path.Combine(programFolder, "config.xml");
        static XElement configFile = XElement.Load(configPath);
        #region Culture Configuration
        public static string GetCulture()
        {
            return configFile.Element("culture").Value.ToString();
        }
        public static void SetCulture(string culture)
        {
            configFile.SetElementValue("lang", culture);
            configFile.Save(configPath);
        }
        public static string GetCultureFolder()
        {
            return Path.Combine(Directory.GetParent(Assembly.GetExecutingAssembly().Location.ToString()).ToString(), configFile.Element("folders").Element("langs").Value.ToString());
        }
        public static string GetCultureFolder(string culture)
        {
            return Path.Combine(Directory.GetParent(Assembly.GetExecutingAssembly().Location.ToString()).ToString(), configFile.Element("folders").Element("langs").Value.ToString(), culture + ".xml");
        }
        public static void SetCultureFolder(string culture)
        {
            configFile.Element("folders").SetElementValue("langs", culture);
            configFile.Save(configPath);
        }
        #endregion
        #region Icons Configuration
        public static string GetIconsFolder()
        {
            return Path.Combine(programFolder, configFile.Element("folders").Element("icons").Value.ToString());
        }
        public static void SetIconsFolder(string folder)
        {
            configFile.Element("folders").SetElementValue("icons", folder);
            configFile.Save(configPath);
        }
        #endregion
    }
}
